namespace RAFTS_TypeProviders

open System
open FSharp.Data.Sql
open System.Data.SqlClient

module MyFirstTypeProvider =
    let helloWorld() = "Hello World!"

    //Connect to SQL Server Database
    let [<Literal>] connectionString = @"Data Source=DDMUGU014109\SQL2014;Initial Catalog=MyDatabase;Integrated Security=SSPI;"
    type dbSchema = SqlDataProvider<ConnectionString = connectionString, UseOptionTypes = true>
    let db = dbSchema.GetDataContext()

    //Query the database
    let getTable1 = 
        query {
            for x in db.Dbo.Table1 do
            select (x.Id, x.TestData1, x.Name, x.TestData2)}
        |> Seq.toArray